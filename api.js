// requires
const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const cors = require('cors')

/**
 * @description port and rute
 */
const port = 3005

/**
 * @description load app
 */
const app = express()
app.use(cors({
  origin: '*'
}))
app.set('port', port)
app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(bodyParser.raw())
app.use(express.urlencoded({ extended: false }))
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Request-Headers', '*')
  res.header('Access-Control-Request-Methods', '*')
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method')
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE')
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE')
  next()
});

app.use('*', (req,res)=>{
    console.log("[METHOD]",req.method);
    console.log("[URL]",req.get('host') + req.originalUrl);
    console.log("[BODY]",req.body);
    console.log("[QUERY]",req.query);
    res.send({})
})

/**
 * app.listen
 * @description enpoint listen
 */
app.listen(port, function () {
  console.info(`[INFO]:: Server running on port ${port}`)
})